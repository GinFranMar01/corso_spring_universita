package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="studente")
public class Studente {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column(name="StudenteID")
	private int StudenteID;
	
	@Column
	private String nome;
	@Column
	private String cognome;
	@Column
	private String data_nascita;
	@Column
	private String matricola;
	public int getStudenteID() {
		return StudenteID;
	}
	public void setStudenteID(int studenteID) {
		StudenteID = studenteID;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getData_nascita() {
		return data_nascita;
	}
	public void setData_nascita(String data_nascita) {
		this.data_nascita = data_nascita;
	}
	public String getMatricola() {
		return matricola;
	}
	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}
	
	
}
