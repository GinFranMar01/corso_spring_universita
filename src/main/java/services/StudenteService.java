package services;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import models.Studente;

@Service
public class StudenteService {
	
	@Autowired
	private EntityManager entMan;
	
	private Session getSessione() {
		return entMan.unwrap(Session.class);
	}
	
	public Studente saveStudente(Studente objStud) {
		
		Studente temp = new Studente();
		temp.setNome(objStud.getNome());
		temp.setCognome(objStud.getCognome()); 
		temp.setMatricola(objStud.getMatricola());
		temp.setData_nascita(objStud.getData_nascita());
		
		Session sessione = getSessione();
		sessione.save(temp);
		
		return temp;
	}
}
