package controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import models.Studente;
import services.StudenteService;

@RestController
@RequestMapping("/studente")
public class StudenteController {
	
	@Autowired
	private StudenteService service;
	
	@GetMapping("/test")
	public String test() {
		return "Benvenuto!";
	}

	@PostMapping("/insert")
	public Studente aggiungi_studente(@RequestBody Studente varStud) {
		return service.saveStudente(varStud);
	}
}
