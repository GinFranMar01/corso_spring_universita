package com.lez18_TASK.gestioneStudente;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestioneStudenteApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestioneStudenteApplication.class, args);
	}

}
